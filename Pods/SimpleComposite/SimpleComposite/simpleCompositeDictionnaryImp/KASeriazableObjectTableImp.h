//
//  KAGenericAttributeContener.h
//  kakebo
//
//  Created by Didier Lobeau on 20/06/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import "KASeriazableObjectImp.h"
#import "KASeriazableObjectTable.h"
#import "KAIterator.h"



@interface KASeriazableObjectTableImp : KASeriazableObjectImp<KASeriazableObjectTable>

+(NSString *) getSelectedAttributeTypeTag;
+(NSString *) getMutiSelectedAllowAttributeTypeTag;

-(id<KASeriazableObject>) scanChildsForItemWithID:(NSString *) ID;
-(id<KASeriazableObject>) scanChildsForItemWithID:(NSString *) ID  WithDynamicallyCreatedChildsInclude:(BOOL) isDynamicChildsInclude;





@end



