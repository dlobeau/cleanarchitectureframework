//
//  KAGenericAttribute.h
//  kakebo
//
//  Created by Didier Lobeau on 10/06/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KASeriazableObject.h"


@interface KASeriazableObjectImp : NSObject <KASeriazableObject>

+(NSString *) getTypeIdTag;
+(NSString *) getLabeLIdentifierTag;
+(NSString *) getGroupIdTag;
+(NSString *) getLabelTag;



+(NSString *) getTypeTag;


+(BOOL) checkDictionnary:(NSDictionary<NSString *,NSString *> *)Dictionnary;

-(id) initWithDictionary:(NSDictionary *) Dictionary;
-(id) initWithLabel:(NSString *) label
             WithLabelIdentifier:(NSString *) ID
        WithObjectFamilyName:(NSString *) GroupId
         WithID:(NSString * ) TypeId;
@end
