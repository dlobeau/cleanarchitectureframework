//
//  KAInjectionDependencyDelegate.h
//  SimpleComposite
//
//  Created by Didier Lobeau on 08/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol KAInjectionDependencyDelegate <NSObject>

-(id) injectedObjectWithParameters:(NSDictionary *) Parameters;

@end

NS_ASSUME_NONNULL_END
