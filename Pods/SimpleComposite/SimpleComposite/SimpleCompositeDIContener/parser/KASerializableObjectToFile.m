//
//  KASerializableObjectToFile.m
//  kakebo
//
//  Created by Didier Lobeau on 07/06/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//

#import "KASerializableObjectToFile.h"
#import "KABasicFile.h"
#import "NSDictionary+KASeriazableObject.h"
#import "NSArray+KASerializableObject.h"

@interface KASerializableObjectToFile()

@property id<KASeriazableObject> toBeSerialize;

@end

@implementation KASerializableObjectToFile


-(id) initWithSerializableObject:(id<KASeriazableObject>) SerializableObject
{
    KASerializableObjectToFile * ReturnValue = [super init];
    
    self.toBeSerialize  = SerializableObject;
    
    return ReturnValue;
}

-(void) serializeToFile:(KABasicFile *) File WithError:(NSError *) Error
{
    NSDictionary * AttributeAsDictionnary = [self.toBeSerialize toDictionnary];
    
    NSMutableArray *Root = [[NSMutableArray alloc] init];
    
    [Root addObject:AttributeAsDictionnary];
    
    NSString *content = [Root toString];
    
    if(content != nil)
    {
        NSString *CompleteFileName = [File getFileCompleteName];
         [content  writeToFile:CompleteFileName
                       atomically:NO
                         encoding:NSUTF8StringEncoding
                            error:&Error];
    
    
        //NSAssert(B, @"Error on File serialization: %@",Error);
    }
}


@end
