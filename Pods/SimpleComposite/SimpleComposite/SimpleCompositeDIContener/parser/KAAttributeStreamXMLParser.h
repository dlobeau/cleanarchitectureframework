//
//  KAAttributeStreamXMLParser.h
//  kakebo
//
//  Created by Didier Lobeau on 08/12/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import "KAAttributeStreamParser.h"

@interface KAAttributeStreamXMLParser : NSObject<KAAttributeStreamParser>

-(id<KAAttributeStreamParser>) initWithString:(NSString *) StringToBeParse;

@property NSString *stringToBeParsed;


@end
