//
//  DcfParserPlistFile.h
//  kakebo
//
//  Created by Didier Lobeau on 07/06/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KAAttributeStreamParser.h"
@protocol KAFile;
@interface KAAttributeStreamParserPlistFile : NSObject <KAAttributeStreamParser>

-(id<KAAttributeStreamParser>) initWithFile:(id<KAFile>) FileToBeParse;

@property id<KAFile>parsedFile;

//@property NSMutableArray<id<KAAttribute>>* controlerList;

@end
