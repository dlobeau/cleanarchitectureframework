//
//  KADcfGenericNode.h
//  kakebo
//
//  Created by Didier Lobeau on 12/06/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KADcfNode.h"


@protocol KASerializeObjectFactory;

@interface KADcfGenericNode : NSObject <KADcfNode>

@property NSString *nodeName;
@property NSDictionary * dictionary;


-(id<KASeriazableObject>) ParseNode:(KADcfGenericNode *) Node WithPath:(NSString *) Path;



@end
