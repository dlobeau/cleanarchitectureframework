//
//  NSArray+KASerializableObject.m
//  kakebo
//
//  Created by Didier Lobeau on 07/06/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//

#import "NSArray+KASerializableObject.h"

@implementation NSArray(KASerializableObject)


-(NSString *) toString
{
    NSString * ReturnValue = nil;
    
    NSError *error;
    
    NSPropertyListFormat format = NSPropertyListXMLFormat_v1_0;
    
    NSData *Data = [NSPropertyListSerialization dataWithPropertyList:self format:NSPropertyListXMLFormat_v1_0 options:format error:&error];
    
    ReturnValue =  [[NSString alloc] initWithData:Data encoding:NSUTF8StringEncoding];
    
    return ReturnValue;
    
}


@end
