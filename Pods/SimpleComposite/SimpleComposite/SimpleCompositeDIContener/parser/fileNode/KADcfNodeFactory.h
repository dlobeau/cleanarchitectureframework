//
//  KADcfNodeFactory.h
//  kakebo
//
//  Created by Didier Lobeau on 12/06/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol KADcfNode;

@interface KADcfNodeFactory:NSObject

-(id<KADcfNode>) createParserNodeFromObject:(id)Object WithNodeName:(NSString *)NodeName;


@end
