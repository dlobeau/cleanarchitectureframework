//
//  DcfFile.h
//  kakebo
//
//  Created by Didier Lobeau on 08/06/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KAFile.h"
@interface KABasicFile : NSObject<KAFile>

@property NSString * completeFilePathAndName;
@property NSString * fileName;

-(id) initWithFileName:(NSString *) FileName WithBundle:(NSBundle *) Bundle;



@end
