//
//  KADataBaseFile.m
//  kakebo
//
//  Created by Didier Lobeau on 09/12/2018.
//  Copyright © 2018 imhuman. All rights reserved.
//

#import "KADataBaseFile.h"

@interface KADataBaseFile()



@end

@implementation KADataBaseFile

-(id) initWithFileName:(NSString *)FileName WithBundle:(NSBundle *)Bundle
{
    KADataBaseFile * ReturnValue = [super init];
    
    self.fileName = FileName;
    
    ReturnValue.completeFilePathAndName = [self copyRessourceFileWithName:FileName];
    
    return ReturnValue;
}
-(NSString * ) copyRessourceFileWithName:(NSString *) FileNameWithoutExtension
{
    NSString *FolderPath = [KADataBaseFile environmentRoot];
    
    return [self copyRessourceFileWithName:FileNameWithoutExtension WithFolderDestination:FolderPath];
}

+(NSString *) environmentRoot
{
    NSString * ReturnValue = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    
    ReturnValue = [NSString stringWithFormat:@"%@/userDataBase",ReturnValue];
    
    return ReturnValue;
}





@end
