//
//  KASerializeObjectLink.h
//  kakebo
//
//  Created by Didier Lobeau on 09/01/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KASeriazableObject.h"
#import "KAGeneralLink.h"

@protocol KASerializeObjectLink <KASeriazableObject,KAGeneralLink>

-(id<KASeriazableObject>) source;
-(void) setSource:(id<KASeriazableObject>) Source;

@end
