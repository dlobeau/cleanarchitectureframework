//
//  View.swift
//  cleanArchitectureFramework
//
//  Created by Didier Lobeau on 20/03/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import UIKit

protocol View {
   //presenter assioaciate to widget
    var interface: Presenter? { get set }

    //called when adding subview to widget
    func add(_ childView: View)

    var widgetID: String { get set }

    //call to refresh UI widget content
    func setContent()

    //signal the presenter data should be sent to domain
    func validateUserChange()
}

extension UIView {

    // TODO: replace with generic parameter
    func isStackViewType() -> (Bool) {
        return self is UIStackView
    }

    func getChild(withID identifier: String) -> (View?) {
        var returnValue: View?
        var viewList = self.subviews
        if viewList.count == 1 {
            let first = viewList[0]
            if first.isStackViewType() {
                viewList = first.subviews
            }
        }
        returnValue = self.getChild(withID: identifier, inList: viewList)

        return returnValue

    }

    private func getChild(withID identifier: String, inList viewList: [UIView]) -> View? {

        for currentView in viewList {
            if currentView.isStackViewType() {
                let viewList2 = currentView.subviews
                if viewList2.count >= 1 {
                    if let returnValue = self.getChild(withID: identifier, inList: viewList2) {
                        return returnValue
                    }
                }
            } else {
                if let returnValue = currentView as? View {
                    let viewID = returnValue.widgetID
                 if viewID == identifier {
                        return returnValue
                    }
                }
            }
        }
        return nil
    }
}
