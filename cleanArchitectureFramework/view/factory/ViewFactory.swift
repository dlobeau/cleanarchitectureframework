//
//  File.swift
//  cleanArchitectureFramework
//
//  Created by Didier Lobeau on 23/03/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation

protocol ViewFactory {

    var identifier: String { get }
    func create(withSender sender: Presenter,
                withOwner owner: View)

}

class ViewFactoryLibrary {

    private var viewFactoryList: [String: ViewFactory] = {
        return [String: ViewFactory]()

    }()

    func add(_ viewFactory: ViewFactory) {
        viewFactoryList[viewFactory.identifier]  = viewFactory
    }

    func get(_ identifier: String) -> (ViewFactory?) {
        return viewFactoryList[identifier]
    }

}
