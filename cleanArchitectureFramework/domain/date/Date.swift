//
//  Date.swift
//  cleanArchitectureFramework
//
//  Created by Didier Lobeau on 22/03/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import SimpleComposite

struct CalendarDate: Domain {
    var label: String = "NO_DATE"
    var identifier: String = "NO_DATE"

   init?(_ identifier: String) {
        self.identifier = identifier
        guard let formatLong = CalendarDateFormatCases.displayedLongFormat.format() else { return }
        guard let date = formatLong.date(from: identifier) else { return }
        self.label = formatLong.string(from: date   )
    }

    init?(withStandardDateObject dateObject: Date) {
        guard let formatShort = CalendarDateFormatCases.internalFormat.format() else { return }
        guard let formatLong = CalendarDateFormatCases.displayedLongFormat.format() else { return }

        self.identifier = formatShort.string(from: dateObject)
        self.label = formatLong.string(from: dateObject)
    }

    lazy var dateObject: Date? = {
        let strDate = self.identifier
        let format = CalendarDateFormatCases.internalFormat.format()
        return format?.date(from: strDate)
    }()

    var dayInMonth: String {
        return String(self.identifier.suffix(2))

    }
}

extension CalendarDate: Equatable {
    static func == (lhs: CalendarDate, rhs: CalendarDate) -> Bool {
        lhs.identifier == rhs.identifier

    }
}

extension CalendarDate: Hashable {

}
