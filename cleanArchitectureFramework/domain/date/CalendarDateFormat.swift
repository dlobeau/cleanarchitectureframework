//
//  CalendarDateFormat.swift
//  cleanArchitectureFramework
//
//  Created by sopra on 26/03/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation

protocol CalendarDateFormat {
    func format () -> DateFormatter?
}

enum CalendarDateFormatCases {
    case internalFormat
    case displayedLongFormat
}

extension CalendarDateFormatCases: CalendarDateFormat {
    func format() -> DateFormatter? {
        guard let identifier = Locale.preferredLanguages.first else { return nil }
        let locale =  Locale.init(identifier: identifier)
        let format = DateFormatter.init()
        format.locale = locale

        switch self {
        case .internalFormat:
            format.setLocalizedDateFormatFromTemplate("yyyy-MM-dd")
            return format
        case .displayedLongFormat:
            format.setLocalizedDateFormatFromTemplate("dd MMMM YYYY")
            return format

        }
    }
}
