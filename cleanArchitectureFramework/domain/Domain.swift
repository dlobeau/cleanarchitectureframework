//
//  Domain.swift
//  cleanArchitectureFramework
//
//  Created by Didier Lobeau on 20/03/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import SimpleComposite

protocol Domain {

    var label: String { get }
    var identifier: String { get  }
    func validate()
    func deleteItem()

    func request(withID requestID: String) -> Domain?
    func modify(withId identifier: String, withValue newValue: Domain)

    func isNull() -> (Bool)
    func isLink() -> (Bool)
}

protocol DomainDataBase: Domain {
    var dataList: [Domain]? { get set }
}

extension Domain {

    func validate() {
    }

    func deleteItem() {
    }

    func request(withID requestID: String) -> Domain? {
        return nil
    }

    func modify(withId identifier: String, withValue newValue: Domain) {

    }

    func isNull() -> (Bool) {
        return false
    }

    func isLink() -> (Bool) {
        return false
    }

}

extension Bool: Domain {
    var label: String {
         return self.identifier

    }

    var identifier: String {
        return String(self)

    }
}

extension String: Domain {
    var label: String {
        return self.identifier

    }

    var identifier: String {
        return String(self)

    }
}
