//
//  cleanArchitectureFramework.h
//  cleanArchitectureFramework
//
//  Created by Didier Lobeau on 20/03/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for cleanArchitectureFramework.
FOUNDATION_EXPORT double cleanArchitectureFrameworkVersionNumber;

//! Project version string for cleanArchitectureFramework.
FOUNDATION_EXPORT const unsigned char cleanArchitectureFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <cleanArchitectureFramework/PublicHeader.h>


