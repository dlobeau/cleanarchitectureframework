//
//  CommandFactory.swift
//  cleanArchitectureFramework
//
//  Created by Didier Lobeau on 23/03/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation

enum CommandCases {

    case labelModification( newLabel: String, sender: Presenter)
    case childModification( newValue: Presenter, sender: Presenter)
    case dateModification( newDate: Date, sender: Presenter)
    case booleanModification( booleanValue: Bool, sender: Presenter)

}

extension CommandCases {
    func createCommand() -> Command {
        switch self {
        case .labelModification(let newLabel, let sender):
            return self.createCommandOnLabelModification(withNewLabel: newLabel, withSender: sender)
        case .childModification(let newValue, let sender):
            return self.createCommandOnChildPresenterModification(withNewSelection: newValue, withSender: sender)
        case .dateModification(let newDate, let sender):
            return self.createCommandOnPresenterModification(withNewDate: newDate, withSender: sender)
        case .booleanModification(let booleanValue, let sender):
            return self.createCommandOnPresenterModification(withBoolean: booleanValue, withSender: sender)

        }
    }
}

protocol CommandFactory {
    func createCommandOnLabelModification(withNewLabel newLabel: String,
                                          withSender sender: Presenter) -> Command
    func createCommandOnChildPresenterModification(withNewSelection newSelection: Presenter,
                                                   withSender sender: Presenter) -> Command
    func createCommandOnPresenterModification(withNewDate newDate: Date,
                                              withSender sender: Presenter) -> Command
    func createCommandOnPresenterModification(withBoolean booleanValue: Bool,
                                              withSender sender: Presenter) -> Command
}

extension CommandCases: CommandFactory {
    func createCommandOnLabelModification(withNewLabel newLabel: String,
                                          withSender sender: Presenter) -> Command {
        return CommandChangeLabel.init(withOwner: sender, withLabel: newLabel)
    }

    func createCommandOnChildPresenterModification(withNewSelection newSelection: Presenter,
                                                   withSender sender: Presenter) -> Command {
        return CommandChangeChildSelectedValue.init(withOwner: sender, withNewChild: newSelection)
    }

    func createCommandOnPresenterModification(withNewDate newDate: Date, withSender sender: Presenter) -> Command {
        return CommandChangeDate.init(withOwner: sender, withNewDate: newDate)
    }

    func createCommandOnPresenterModification(withBoolean booleanValue: Bool, withSender sender: Presenter) -> Command {
        return CommandChangeBooleanValue.init(withOwner: sender, withBooleanValue: booleanValue)
    }

}
