//
//  Command.swift
//  cleanArchitectureFramework
//
//  Created by Didier Lobeau on 20/03/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation

protocol Command {
    func doCommand()
}

struct CommandChangeDate: Command {

    var dateNewValue: Date
    var owner: Presenter

    init(withOwner owner: Presenter, withNewDate newDate: Date) {
        self.owner = owner

        self.dateNewValue = newDate

    }

    func doCommand () {

        self.owner.data = CalendarDate.init(withStandardDateObject: dateNewValue)
    }

}

struct CommandChangeLabel: Command {

    var label: String
    var owner: Presenter

    init(withOwner owner: Presenter, withLabel newLabel: String) {
        self.owner = owner

        self.label = newLabel

    }

    func doCommand () {
        self.owner.data = self.label
    }

}

struct CommandChangeBooleanValue: Command {
    var boolValue: Bool
    var owner: Presenter

   init(withOwner owner: Presenter, withBooleanValue booleanValue: Bool) {
    self.owner = owner

        self.boolValue = booleanValue

    }

    func doCommand () {

        self.owner.data = self.boolValue
    }

}

struct CommandChangeChildSelectedValue: Command {

    var newChild: Presenter
    var owner: Presenter

    init(withOwner owner: Presenter, withNewChild newChild: Presenter) {
        self.owner = owner

        self.newChild = newChild

    }

    func doCommand () {
        guard let data = self.newChild.data else { return }
        self.owner.data?.modify(withId: self.newChild.id(), withValue: data)

    }

}
