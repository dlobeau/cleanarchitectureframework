//
//  CheckBox.swift
//  cleanArchitectureFramework
//
//  Created by sopra on 02/04/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation

protocol CheckBox: Presenter {
    var value: Bool { get set }
}

/*
class CheckBoxPresenter: PresenterComposite, CheckBox {

-(id) initWithLabel:(NSString *)label WithLabelIdentifier:(NSString *)ID
 WithObjectFamilyName:(NSString *)GroupId WithID:(NSString *)TypeId
{
    self = [ super initWithLabel:label WithLabelIdentifier:ID WithObjectFamilyName:GroupId WithID:TypeId];
    self.testdelegate = [[KACheckBoxPresenterTest alloc] init];
    return self;
}

-(BOOL)value
{
    id<KABoolean> Status =(id<KABoolean>) [self data];
   
    return Status.value;
}

+(NSString *) getTypeTag
{
    return @"checkBox";
}




}


struct CheckBoxPresenterExpectation: PresenterExpectation {

    func checkDomain(withSender presenter: CheckBox, withReference reference: Domain) {
        assert(reference.label! == presenter.properties.text!,
               "\(presenter.properties.text!) instead of \(reference.label!)")
    }

    func checkText(withSender presenter: Presenter, withReference reference: String) {
        assert(reference == presenter.properties.text!,
               "\(presenter.properties.text!) instead of \(reference)")
    }

}

@implementation KACheckBoxPresenterTest

-(void) expectSender:(id<KACheckBox>)Sender conformToDomain:(id<KABoolean>)Domain
{
   
    NSAssert(Sender.value == Domain.value, @"Error in data binding, check %@",Sender.attributeIdentity);
}*/
