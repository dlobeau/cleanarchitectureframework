//
//  Label.swift
//  cleanArchitectureFramework
//
//  Created by sopra on 28/03/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation

protocol Label {

}

class LabelPresenter: PresenterComposite, Label {

    override class func getTypeTag() -> String! {
        return "Label"
    }

    override init!(dictionary presenterDictionary: [AnyHashable: Any]!) {
        super.init(dictionary: presenterDictionary)

    }

    override func expectConformity(withDomain domain: Domain) {
        assert(domain.label == self.properties.text!,
        "\(self.properties.text!) instead of \(domain.label)")
    }
}
