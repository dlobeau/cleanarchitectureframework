//
//  Cell.swift
//  cleanArchitectureFramework
//
//  Created by sopra on 27/03/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation

protocol Cell: Presenter {
    var swipLeftEvents: [CellSwipAction]? { get }
}

class CellPresenter: PresenterComposite, Cell {

    override class func getTypeTag() -> String! {
        return "cell"
    }

    lazy var swipLeftEvents: [CellSwipAction]? = {
        guard let list = self.subPresenters else { return nil }

        var returnValue: [CellSwipAction]?
        for currentPresenter in list {
            if let currentAction = currentPresenter as? CellSwipAction {
                if returnValue == nil {
                    returnValue = [CellSwipAction]()
                }
                returnValue?.append(currentAction)
            }
        }
        return returnValue
    }()

}

protocol CellSwipAction: Presenter {

}

class CellSwipActionPresenter: PresenterComposite, CellSwipAction {

    override var isInitializedOnParentCreation: Bool {
        return false
    }

}
