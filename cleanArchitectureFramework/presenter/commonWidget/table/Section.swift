//
//  Section.swift
//  cleanArchitectureFramework
//
//  Created by sopra on 27/03/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation

protocol Section: Presenter {
    var cells: [Cell]? { get }
    func cell(atIndex index: Int) -> Cell?
}

class SectionPresenter: PresenterComposite, Section {
    override class func getTypeTag() -> String! {
        return "section"
    }

   override var isInitializedOnParentCreation: Bool {
        return false
    }

    var cells: [Cell]? {
        var returnValue: [Cell]?

        guard let allCHilds = self.subPresenters else { return nil }

        for currentCell in allCHilds {
            if let currentCell = currentCell as? Cell {
                if returnValue == nil {
                    returnValue = [Cell]()
                }
                returnValue?.append(currentCell)
            }

        }
        return returnValue

    }

    func cell(atIndex index: Int) -> Cell? {
        guard let cells = self.cells else { return nil }
        guard index < cells.count else { return nil }
        return cells[index]
    }
}
