//
//  Table.swift
//  cleanArchitectureFramework
//
//  Created by sopra on 27/03/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation

protocol Table: Presenter {

}

class TablePresenter: PresenterComposite, Table {

    override class func getTypeTag() -> String! {
        return "table"
    }

    var sections: [Section]? {
        var returnValue: [Section]?
        guard let allCHilds = self.subPresenters else { return nil }

        for currentPresenter in allCHilds {
            if let currentSection = currentPresenter as? Section {
                if returnValue == nil {
                    returnValue = [Section]()
                }
                returnValue?.append(currentSection)
            }
        }
        return returnValue
    }

    var cells: [Cell]? {
        var returnValue: [Cell]?
        guard let sections = self.sections else { return nil }

        for currentSection in sections {
            if let cells = currentSection.cells {
                if returnValue == nil {
                    returnValue = [Cell]()
                }
                returnValue?.append(contentsOf: cells)
            }

        }
        return returnValue
    }

    func section(AtIndex index: Int) -> (Section?) {
        guard let sections = self.sections else { return nil }
        guard sections.count > index else { return nil }

        return sections[index]
    }

}
