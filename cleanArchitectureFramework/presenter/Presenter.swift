//
//  Presenter.swift
//  cleanArchitectureFramework
//
//  Created by Didier Lobeau on 20/03/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import UIKit
import SimpleComposite

// MARK: Commandable protocol

protocol Commandable {

    func executeCommandHierarchy()
    func clearCommandList()

    func addCommandOnLabelModification(withNewLabel newLabel: String)
    func addCommandOnChildSelectionModification(withNewSelection newSelection: Presenter)
    func addCommandOnDateModification(withNewDate newDate: Date)
    func addCommandOnBooleanModification(withBoolean booleanValue: Bool)
}

// MARK: PresenterProperty protocol

struct PresenterProperties {
    //will tell the related UI widget if it can be enabled
    var isEnabled = true

    //will tell the related UI widget if it is visible
    var isVisible = true

    var text: String?

    //will tell the realted UI which color to be defined with
    var color = -1

    //will tell the related UI what should be it size
    var height = 0
    var width = 0

    //will tell the related UI what image it should display
    var image: UIImage?

    var isStaticWidget = true
}

// MARK: PresenterDynamicWidgetManager protocol

protocol PresenterDynamicWidgetManager {
    func dynamicSubpresenters(withTemplate template: Presenter) -> [Presenter]?

}

protocol Testable {
    func expectConformity (withDomain domain: Domain)

}

// MARK: Presenter protocol

protocol Presenter: KACompleteItemIdentification, KAComparable, Testable {
    //domain the presenter is initialized with
    var data: Domain? { get set }

    var event: Event? { get }

    var query: Query? { get }

    var properties: PresenterProperties { get set }

    var subPresenters: [Presenter]? { get }
    func subPresenter(withId identifier: String) -> (Presenter?)
    var parent: Presenter? { get set }

    //create UI associated widget
    func createView(withOwner owner: View)

    //refresh presenter data from domain
    func dataBinding()
    func asynchronousDataBinding( _ completion: () -> Void )

    //update domain with presenter data
    func validateChange()

    //rather widget view should be created with parents creation
    //or after via delegate (like cells or sections for UITableView)
    var isInitializedOnParentCreation: Bool { get }
}

// MARK: PresenterComposite

class PresenterComposite: KASeriazableObjectTableImp, Presenter, Testable {

    var data: Domain? {
        didSet {
            self.dataBinding()
            //TODO: add call to here [self.dataModificationDelegate
            //modificationOccuredOnObservedPresenterWithSender:self];

        }
    }

    var properties = PresenterProperties()

    lazy var commands: [Command] = [Command]()

    var query: Query?

    var event: Event?

    var childPresenter: [Presenter]?

    var parent: Presenter?

    var isInitializedOnParentCreation: Bool {
        return true
    }

    var viewFactory: ViewFactory?

    private var subPresentersCached: [Presenter]?

    var subPresenters: [Presenter]? {
        if subPresentersCached == nil {
            subPresentersCached = [Presenter]()
            let iterator = self.getIterator()
            while iterator.hasNext() {
                if let  currentChild = iterator.next() as? Presenter {
                    if !currentChild.properties.isStaticWidget,
                        let presenterList = self.dynamicSubpresenters(withTemplate: currentChild) {
                        subPresentersCached?.append(contentsOf: presenterList )
                    } else if currentChild.properties.isStaticWidget {
                        subPresentersCached?.append(currentChild)
                    }
                }
            }
        }
        return subPresentersCached
    }

    func subPresenter(withId identifier: String) -> (Presenter?) {
        let presenter = self.getChildwithTypeId(identifier) as? Presenter

        return presenter
    }

    func dataBinding() {
        if let queryData = dataFromQuery { self.data = queryData }

        self.resetCache()

        guard let subpresenters = self.childPresenter else { return }

        for currentInterface in  subpresenters
            where currentInterface.properties.isStaticWidget {
                var childData = self.data?.request(withID: currentInterface.id())

                if childData == nil {
                    childData = self.data

                currentInterface.data = childData
            }
        }
    }

    private var dataFromQuery: Domain? {
       guard let query = self.query else { return nil }
       query.execute()
       return query.result
    }

    func resetCache() {
        self.subPresentersCached?.removeAll()
    }

    func asynchronousDataBinding(_ completion: () -> Void) {

    }

    func createView(withOwner owner: View) {

    }

    func validateChange() {

    }

    func expectConformity (withDomain domain: Domain) {

    }

}

// MARK: PresenterDynamicWidgetManager

extension PresenterComposite: PresenterDynamicWidgetManager {
    func dynamicSubpresenters(withTemplate template: Presenter) -> [Presenter]? {
        guard  let parent = template.parent else { return nil }

        assert( parent === self, "can't retrieve dynamic child from other node parents")
        assert(!template.properties.isStaticWidget,
               "Check : \(template.attributeIdentity()), node is static")

        var returnValue = [Presenter]()

        guard let dataTable = self.data?.request(withID: template.id()) as? DomainDataBase else { return nil }
        guard let dataList = dataTable.dataList else { return nil  }

        for currentData in dataList {
                if let newWidget = template.cloneObject() as? Presenter {
                    newWidget.setID(currentData.identifier)
                    newWidget.parent = self
                    newWidget.data = currentData
                    returnValue.append(newWidget)
                }
            }
        return returnValue
    }

}

// MARK: CommandAction

extension PresenterComposite: Commandable {

    func addCommandOnLabelModification(withNewLabel newLabel: String) {
        self.commands.append(CommandCases.labelModification(newLabel: newLabel, sender: self).createCommand())
    }

    func addCommandOnChildSelectionModification(withNewSelection newSelection: Presenter) {
        self.commands.append(CommandCases.childModification(newValue: newSelection, sender: self).createCommand())
    }

    func addCommandOnDateModification(withNewDate newDate: Date) {
        self.commands.append(CommandCases.dateModification(newDate: newDate, sender: self).createCommand())
    }

    func addCommandOnBooleanModification(withBoolean booleanValue: Bool) {
        self.commands.append(CommandCases.booleanModification(booleanValue: booleanValue,
                                                              sender: self).createCommand())
    }

    func executeCommandHierarchy() {
        self.commands.forEach {
            $0.doCommand()
        }
    }

    func clearCommandList() {
        self.commands.removeAll()
    }

}
