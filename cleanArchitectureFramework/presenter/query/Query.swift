//
//  Query.swift
//  cleanArchitectureFramework
//
//  Created by Didier Lobeau on 22/03/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import SimpleComposite

protocol Query {
    var criterias: [Criteria]? { get }
    var repository: Domain? { get }

    func execute()

    var result: Domain? { get set }
}

class QueryComposite: KASeriazableObjectTableImp, Query {
    var queryResult: Domain?
    var result: Domain?
    override class func getTypeTag() -> String! {
       return "query"
    }

    var criterias: [Criteria]? {
        var returnValue: [Criteria]?
        guard let childs = self.childs() else { return nil }
        for currentChild in childs {
            if let criteria = currentChild as? Criteria {
                if returnValue == nil {
                    returnValue = [Criteria]()
                }
                returnValue?.append(criteria)
            }
        }
        return returnValue
    }

    var repository: Domain? {
        return self.getChildwithTypeId("repository") as? Domain

    }

    func execute() {
        guard let criteria = self.criterias?[0] else { return }
        guard let repository = self.repository else { return }
        guard let useCaseNode = criteria.queryBusinessUseCase else { return }
        guard let value = criteria.value else { return }
        guard let useCase = UseCaseLibrary.get(useCaseNode.label) else { return }

        self.result = useCase.execute(withRepository: repository, withValue: value)
    }
}
