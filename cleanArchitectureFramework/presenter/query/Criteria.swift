//
//  Criteria.swift
//  cleanArchitectureFramework
//
//  Created by Didier Lobeau on 22/03/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import SimpleComposite

protocol Criteria {
    var queryBusinessUseCase: Domain? { get }
    var value: Domain? { get }

}

class CriteriaSimpleComposite: KASeriazableObjectTableImp, Criteria {

    override class func getTypeTag() -> String! {
        return "criteria"
    }

   var queryBusinessUseCase: Domain? {
        guard let useCaseNode = self.getChildwithTypeId("useCase") as? Domain else { return nil }
        return useCaseNode
    }

   var value: Domain? {
        return self.getChildwithTypeId("value") as? Domain

    }

}
