//
//  UseCase.swift
//  cleanArchitectureFramework
//
//  Created by Didier Lobeau on 22/03/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation

protocol UseCase {
    var idenfication: String { get }
    func execute(withRepository repository: Domain, withValue value: Domain) -> (Domain?)
}

class UseCaseLibrary {

    static private var useCaseList: [String: UseCase] = {
        var returnValue = [String: UseCase]()
         return returnValue
    }()

    static func add(_ useCase: UseCase) {
        useCaseList[useCase.idenfication] = useCase
    }

    static func get(_ identification: String) -> (UseCase?) {
        let result = useCaseList[identification]
        return result
    }
}

struct UseCaseNoAction: UseCase {
    var idenfication = "noAction"
    func execute(withRepository repository: Domain,
                 withValue value: Domain) -> (Domain?) {
        return nil
    }

}
