//
//  PresenterExpectation.swift
//  cleanArchitectureFramework
//
//  Created by Didier Lobeau on 24/03/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation

protocol PresenterExpectation {
    func checkDomain (withPresenter presenter: Presenter, withReference reference: Domain)
    func checkText (withPresenter presenter: Presenter, withReference reference: String)
}

extension PresenterExpectation {
    func checkDomain (withPresenter presenter: Presenter, withReference reference: Domain) {

    }
    func checkText (withPresenter presenter: Presenter, withReference reference: String) {

    }
}
