//
//  Event.swift
//  cleanArchitectureFramework
//
//  Created by Didier Lobeau on 20/03/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation

protocol Event: Presenter {
}

protocol EventNewPage: Event {
    func destination(withOwner owner: Presenter) -> (Presenter?)
}

protocol EventActionExecution: Event {
    func doAction(_ sender: Presenter)
}

class EventNewPagePresenter: PresenterComposite, EventNewPage {
    let destinationTag = "destination"
    let commandTag = "command"

    override class func getTypeTag() -> String! {
        return "eventNewPage"
    }

    func destination(withOwner owner: Presenter) -> (Presenter?) {
        guard let returnValue =  super.getChildwithTypeId(destinationTag) as? Presenter else { return  nil }
        returnValue.data = self.destinationData(withContext: owner)

        return returnValue
    }

    private func destinationData(withContext  context: Presenter) -> (Domain?) {
        guard let data = self.getChildwithTypeId("data") as? Domain else { return nil }
        return data
    }
}

class EventDoAction: PresenterComposite, EventActionExecution {
    override class func getTypeTag() -> String! {
           return "eventAction"
    }

    func doAction(_ sender: Presenter) {guard let data = self.getChildwithTypeId("data") as? Domain else { return }
        data.validate()
    }
}
