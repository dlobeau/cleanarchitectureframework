//
//  Application.swift
//  cleanArchitectureFramework
//
//  Created by sopra on 27/03/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import SimpleComposite

class ApplicationManager: PresenterComposite {
    lazy var bundle: Bundle? = {
        return Bundle.main
    }()

    lazy var viewFactoryLibrary: ViewFactoryLibrary = {
           return ViewFactoryLibrary()
       }()

    func start() {

        self.registerUseCases()
        self.registerViewFactory()
    }

    func registerUseCases() {
        UseCaseLibrary.add(UseCaseNoAction())
    }

    func registerViewFactory() {

    }

    lazy var window: Presenter? = {
        guard let list = self.subPresenters else { return nil }
        guard !list.isEmpty else { return nil }
        return list[0]
    }()

}
